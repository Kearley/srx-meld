#!/usr/bin/python3
'''
Junos Get Feed
Fetches an standard IP feed file from a web url (such as MineMeld) and reformats to a new file
for Junos SRX consumption.  Generates the associated manifest.xml and schema.xml required for SRX
to download and process.

SRX file formating inspired by this project:
https://github.com/farsonic/dynamic-address

Note: for Junos SRX post 15.1x49, the device appends '?device=######' to the URI for fetching the 
manifest.xml.  Your web server hosting the feed files needs to ignore or rewrite this.

Author: dkearley@integrationpartners.com

'''

#################
# Imports 
#################
import requests
import urllib3
import netaddr
import time
from pprint import pprint as pp
from jinja2 import Environment, FileSystemLoader, Template 
import os, yaml
import hashlib
import shutil


#################
# Globals 
#################
# feed_file = 'myfeeds.txt'
feed_file = 'feeds-test.txt'
feed_location ='/var/www/html/'
stage_location = 'stage/'
temp_location ='tmp/'

#################
# Functions 
#################

def file_render(template, data, file_name):
	''' 
	Renders file using jinja2 templates. 
	Takes a template name, a dictionary, and a file name as input. 
	Writes the rendered file to stage_location. 
	Returns nothing.
	'''
	my_template = Environment(loader=FileSystemLoader(searchpath='.')).get_template(template)
	ts = int(time.time())
	my_template.globals['ts'] = ts 
	rendered_file = my_template.render(data)

	with open(stage_location + file_name, 'w') as file:
	    file.write(rendered_file)
	    return(file.name)


def append_md5(file_name):
	''' 
	Generates an MD5 hash. 
	Takes a file name as input. 
	Opens the file, generates an MD5 hash, then appends the hash to the same file.
	Assumes the file is in the stage_location.
	Returns nothing. 
	'''
	with open(stage_location + file_name) as file:
		data = file.read()
		md5_returned = hashlib.md5(data.encode("utf-8")).hexdigest()
		file.close()

	file = open(stage_location + file_name,'a')
	file.write(md5_returned)
	file.close()

def get_feeds_list(file_name):
	# Get URL Feeds in feed_file
	with open(file_name) as file:
		feeds_url_list = file.readlines()

	# Cleanup any whitespaces and newlines and get a feed count
	feeds_url_list = [x.strip() for x in feeds_url_list]
	feed_count = len(feeds_url_list)

	return(feeds_url_list, feed_count)

def render_manifest(feed_name_list):
	manifest_dict = dict()
	# Add feeds_names to feed_dict and call file_render() for the manifest.xml
	manifest_dict['feeds'] = feed_name_list
	file = file_render('manifest.j2',manifest_dict,'manifest.xml')
	return(file)

def process_feeds(feeds_url_list, feed_count):
	# # Open the users feed_file to get feed URLs and counts
	# feeds_url_list, feed_count = get_feeds_list(feed_file)
	# print("{} feeds found in: '{}'\n".format(feed_count, feed_file))

	# Establish an empty list to store all processed feed names
	feed_name_list = list()

	# Set a counter for reporting progress to the user while looping
	index = 1

	# Process each feed found
	for url in feeds_url_list:
		feeds_dict = dict()
		entries = list()

		# Debug
		# print(url)

		# Get feed name, append to feed_name_list, and get feed content
		feed_name = url.split('/')[-1]
		feed_name_list.append(feed_name)
		print("Retrieving content of feed {} of {} @ {}".format(index,feed_count,url))
		response = requests.get(url, allow_redirects=True, verify=False)

		# Make sure the given URL works!  Any thing but a HTTP return code 200, raise the error
		try: 
			response.raise_for_status()
		except requests.exceptions.HTTPError as e:
			raise SystemExit(e)

		# Write to file for safe keeping/documentation/troubleshooting
		feed_name_temp = feed_name + ".temp"
		print("Writing feed contents of '{}' to file '{}'".format(feed_name, temp_location + feed_name_temp))
		open(temp_location + feed_name_temp, 'wb').write(response.content)

		# Open the copied feed file and read line by line
		print("Beginning conversion operations for '{}'.".format(feed_name_temp))
		feed = open(temp_location + feed_name_temp, 'r')
		lines = feed.readlines()

		for line in lines:
			# Debug
			# print(line)

			# if not line.startswith('#'):
			try: 
				# Look for ranges and iterate them
				if "-" in line:
					start_ip,end_ip = line.split("-")

					# If start and end IPs are the same, we have a host address (1)
					if netaddr.IPNetwork(start_ip) == netaddr.IPNetwork(end_ip):
						ip = netaddr.IPNetwork(start_ip)
						value = ip.value
						size = ip.size
						entry = '{"1":'+str(value)+'}'
						entries.append(entry)

					# If they are different, treat as an IP range (2) and figure out the hosts count 
					else:
						ip_set_object = netaddr.IPSet(netaddr.IPRange(start_ip, end_ip, flags=netaddr.ZEROFILL))
						hostcount = len(ip_set_object)-1
						ip = netaddr.IPRange(start_ip,end_ip, flags=netaddr.ZEROFILL)
						value = ip.first
						entry = '{"2":['+str(value)+','+str(hostcount)+']}'
						entries.append(entry)
				# Look for prefixes and render them
				if "/" in line:
					ip = netaddr.IPNetwork(line)
					value = ip.value
					size = ip.size
					entry = '{"1":'+str(value)+'}'
					entries.append(entry)
			except:
				continue
		index += 1
		# print(len(entries))
		# pp(entries)
		feeds_dict['entries'] = entries
		# pp(feeds_dict)
		file = file_render('feed.j2',feeds_dict, feed_name)
		append_md5(feed_name)
		print("Conversion completed and saved to '{}'\n".format(file))

	# Render the manifest.xml
	print("Generating SRX feed manifest.")
	manifest = render_manifest(feed_name_list)
	print("SRX feed manifest saved to '{}'".format(manifest))
	return()


def main():
	# Disable SSL cert warnings
	urllib3.disable_warnings()

	# Start the clock for fun
	test_start = time.time()

	# Open the users feed_file to get feed URLs and counts
	feeds_url_list, feed_count = get_feeds_list(feed_file)
	print("{} feeds found in: '{}'\n".format(feed_count, feed_file))

	# Pass the feeds_url_list to process_feeds() for processing
	process_feeds(feeds_url_list, feed_count)

	# Publish the files in stage_location to the web server directory

	# Stop the clock and repor execution time
	print('\nTotal execution time:', time.time()-test_start,'seconds\n')
	print('Call it', round(time.time()-test_start,3),'seconds...\n')

#################
# Main 
#################
if __name__ == "__main__":
    main()
