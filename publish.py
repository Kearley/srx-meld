
import shutil, os

#################
# Globals 
#################
feed_file = 'feeds-test.txt'
feed_location ='/var/www/html/'
stage_location = 'stage/'
temp_location ='tmp/'

def main():

	# Get the filenames in the in_dir including their path as save to a list
	filepaths = list()
	files = os.listdir(stage_location)
	for file in files:
		print(file)
		print(stage_location + file)
	# 	mypath = os.path.join(stage_location,file)
	# 	if os.path.isfile(mypath):
	# 		filepaths.append(mypath)
	# # Copy to the web servers feed_location
	# for file in filepaths:
		shutil.copyfile(stage_location + file, feed_location + file)


#################
# Main 
#################
if __name__ == "__main__":
    main()
